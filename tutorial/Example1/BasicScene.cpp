#include <algorithm>
#include <assert.h>
#include <utility>

#include "BasicScene.h"
#include "IglMeshLoader.h"
#include "ObjLoader.h"
#include "igl/edge_flaps.h"
#include "igl/read_triangle_mesh.cpp"
#include <igl/circulation.h>
#include <igl/collapse_edge.h>
#include <igl/edge_collapse_is_valid.h>
#include <igl/edge_flaps.h>
#include <igl/parallel_for.h>
#include <igl/per_face_normals.h>
#include <igl/per_vertex_normals.h>
#include <igl/shortest_edge_and_midpoint.h>
#include <read_triangle_mesh.h>

bool invertble;
using namespace cg3d;

void BasicScene::Init(float fov, int width, int height, float near, float far) {
  camera =
      Camera::Create("camera", fov, float(width) / float(height), near, far);
  auto program = std::make_shared<Program>("shaders/basicShader");
  auto material =
      std::make_shared<Material>("material", program); // empty material
  auto cubeMesh{IglLoader::MeshFromFiles("cube_igl", "data/bunny.off")};

  igl::read_triangle_mesh("data/bunny.off", V, F);
  igl::edge_flaps(F, E, EMAP, EF, EI);
  meshes.emplace_back(cubeMesh);

  calculate_Q_matrices();
  calculate_optimal_edges_contraction();
  calculate_edges_cost();

  cube = Model::Create("cube", cubeMesh, material);
  AddChild(cube);

  camera->Translate(15, Axis::Z);
  cube->Scale(3);
}

void BasicScene::SetCamera(int index) {
  camera = camList[index];
  viewport->camera = camera;
}

void BasicScene::SetActive(bool _isActive) { animate = _isActive; }

void BasicScene::calculate_Q_matrices()
// Calculate and store in `verticesQ` the value of Q matrix per each vertex.
// The error/cost at each vertex `v` is equal to: v^T * Q * v.
{
  igl::per_face_normals(V, F, faceNormals);
  std::cout << "Face Normals: " << faceNormals << std::endl;
  verticesQ = std::vector<Eigen::Matrix4d>(V.rows(), Eigen::Matrix4d::Zero());

  for (auto i = 0; i < F.rows(); i++) {
    // p = [a, b, c, d]^Transpose, and [a, b, c] == normal of the face/plane.
    // According to lecture slides, ax + by +cz + d = 0.
    const auto vertexOnFace =
        V.row(F.row(i)[0]); // Coordinatex of a vertex on the current face.
    const auto faceNormal = faceNormals.row(i);
    const auto d = -(faceNormal * vertexOnFace.transpose());
    assert(d.size() == 1);

    Eigen::Vector4d p(faceNormal.x(), faceNormal.y(), faceNormal.z(), d(0, 0));
    const auto Kp = p * p.transpose(); // K_p matrix of current face/plane.

    // Add K_p matrix to Q matrix of all vertices incident with the current
    // face.
    for (auto j = 0; j < F.row(i).cols(); j++) {
      auto vertexIndex = F.row(i).col(j);
      assert(vertexIndex.size() == 1);
      verticesQ.at(vertexIndex(0, 0)) += Kp;
    }
  }
}

void BasicScene::calculate_optimal_edges_contraction()
// Calculate and store in `edgeContractions` the optimal contraction
// point of an edge (where the new vertex will be after contraction).
// This uses the Q matrix of each vertex to calculate the optimal point.
// For further info on this calculation, see section 4 of the paper.
{
  edgeContractions.resize(E.rows(), 3);

  for (int i = 0; i < E.rows(); i++) {
    const auto vertexAIndex = E.row(i)(0);
    const auto vertexBIndex = E.row(i)(1);

    Eigen::Matrix4d newVertexQ =
        verticesQ.at(vertexAIndex) + verticesQ.at(vertexBIndex);
    newVertexQ.row(3) = Eigen::Vector4d(
        0, 0, 0, 1); // Replace last row with (0, 0, 0, 1) vector.
    bool invertible = false;
    double determinant;
    Eigen::Matrix4d inverse;
    newVertexQ.computeInverseAndDetWithCheck(inverse, determinant, invertble);

    if (!invertible) {
      // Midpoint error.
      const auto middlePoint = (V.row(vertexAIndex) + V.row(vertexBIndex)) / 2;
      Eigen::Vector4d middlePointPadded(middlePoint.x(), middlePoint.y(),
                                        middlePoint.z(), 1);
      const auto middleQ =
          verticesQ.at(vertexAIndex) + verticesQ.at(vertexBIndex);
      const auto middleError =
          (middlePointPadded.transpose() * middleQ * middlePointPadded)(0, 0);

      // Vertex A error.
      const auto vertexA = V.row(vertexAIndex);
      const Eigen::Vector4d vertexAPadded(vertexA.x(), vertexA.y(), vertexA.z(),
                                          1);
      const auto &vertexAQ = verticesQ.at(vertexAIndex);
      const auto vertexAError =
          (vertexAPadded.transpose() * vertexAQ * vertexAPadded)(0, 0);

      // Vertex B error.
      const auto vertexB = V.row(vertexBIndex);
      const Eigen::Vector4d vertexBPadded(vertexB.x(), vertexB.y(), vertexB.z(),
                                          1);
      const auto &vertexBQ = verticesQ.at(vertexBIndex);
      const auto vertexBError =
          (vertexBPadded.transpose() * vertexBQ * vertexBPadded)(0, 0);

      if (vertexAError < vertexBError && vertexAError < middleError) {
        edgeContractions.row(i) = vertexA;
        continue;
      }

      if (vertexBError < vertexAError && vertexBError < middleError) {
        edgeContractions.row(i) = vertexB;
        continue;
      }

      if (middleError < vertexBError && middleError < vertexAError) {
        edgeContractions.row(i) = middlePoint;
        continue;
      }
    }

    auto res = inverse * Eigen::Vector4d(0, 0, 0, 1);
    Eigen::Vector3d newVertex =
        res.topRows(3); // Discard last row which is just a `1` for padding.
    edgeContractions.row(i) = newVertex;
  }
}

// NOTE/WARNING: make sure to call calculate_optimal_edges_contraction()
// before calling this function.
// This is because an edge's cost depends on the contraction point of the edge.
void BasicScene::calculate_edges_cost() {
  edgeCosts.clear();
  edgeCostsE.clear();

  for (int i = 0; i < E.rows(); i++) {
    const auto vertexAIndex = E.row(i)(0);
    const auto vertexBIndex = E.row(i)(1);

    Eigen::Matrix4d newVertexQ =
        verticesQ.at(vertexAIndex) + verticesQ.at(vertexBIndex);
    Eigen::Vector3d newVertex = edgeContractions.row(i);
    Eigen::Vector4d newVertexPadded(newVertex.x(), newVertex.y(), newVertex.z(),
                                    1);
    auto edgeCost = newVertexPadded.transpose() * newVertexQ * newVertexPadded;
    assert(edgeCost.size() == 1);

    edgeCostsE.push_back(std::pair(edgeCost(0, 0), i));
    // TODO: why push_back and not emplace_back?
    edgeCosts.push_back(
        std::pair(edgeCost(0, 0), std::pair(vertexAIndex, vertexBIndex)));
  }

  std::sort(edgeCostsE.begin(), edgeCostsE.end(),
            [](auto a, auto b) { return a.first > b.first; });
  std::sort(edgeCosts.begin(), edgeCosts.end(),
            [](auto a, auto b) { return a.first > b.first; });
}

// TODO: if can't collapse anymore return false.
bool BasicScene::collapse_edge() {
  auto edge_w_cost = edgeCostsE.back();
  edgeCostsE.pop_back();
  int vertexAIndex = E.row(edge_w_cost.second)(0);
  int vertexBIndex = E.row(edge_w_cost.second)(1);
  int edgeIndex = edge_w_cost.second;

  if (!igl::edge_collapse_is_valid(edgeIndex, F, E, EMAP, EF, EI)) {
    return false;
  }

  // delete edge
  removeRow(E, edgeIndex);

  // delete faces (all faces that include both vertexA and vertexB)
  std::vector<int> to_remove;
  for (int i = 0; i < F.rows(); ++i) {
    bool to_remove_face = false;
    for (int j = 0; j < 3; ++j)
      if (F.row(i)(j) == vertexBIndex)
        F.row(i)(j) = vertexAIndex;
    for (int j = 0; j < 3; ++j)
      for (int k = 0; k < 3; ++k)
        if (j != k &&
            ((F.row(i)(j) == vertexAIndex && F.row(i)(k) == vertexBIndex) ||
             (F.row(i)(j) == F.row(i)(k))))
          to_remove_face = true;
    if (to_remove_face)
      to_remove.insert(to_remove.begin(), i);
  }
  for (int i = 0; i < to_remove.size(); ++i)
    removeRow(F, to_remove.at(i));

  // merge vertices to vertexA
  for (int i = 0; i < 3; ++i)
    V.row(vertexAIndex)(i) = edgeContractions.row(edgeIndex)(i);
  for (int i = 0; i < E.rows(); i++) {
    if (E.row(i)(0) == vertexBIndex) { //(B, __)
      E.row(i)(0) = vertexAIndex;
    }
    if (E.row(i)(1) == vertexBIndex) //(__, B)
      E.row(i)(1) = vertexAIndex;
  }
  removeRow(V, vertexBIndex);

  // remove duplicates
  to_remove.clear();
  for (int i = 0; i < E.rows(); ++i)
    for (int j = i + 1; j < E.rows(); ++j)
      if (E.row(i)(0) == E.row(j)(0) && E.row(i)(1) == E.row(j)(1) ||
          E.row(i)(0) == E.row(j)(1) && E.row(i)(1) == E.row(j)(0))
        to_remove.insert(to_remove.begin(), i);
  for (int i = 0; i < to_remove.size(); ++i) {

    removeRow(E, to_remove.at(i));
  }

  // rename edges and faces according to new vertices
  for (int i = 0; i < E.rows(); i++) {
    for (int j = 0; j < 2; j++) {
      if (E.row(i)(j) > vertexBIndex)
        E.row(i)(j)--;
    }
  }
  for (int i = 0; i < F.rows(); i++) {
    for (int j = 0; j < 3; j++) {
      if (F.row(i)(j) > vertexBIndex)
        F.row(i)(j)--;
    }
  }

  std::cout << "edge " << edgeIndex << ", cost = " << edge_w_cost.first
            << ", new v position (" << edgeContractions.row(edgeIndex)(0) << ","
            << edgeContractions.row(edgeIndex)(1) << ","
            << edgeContractions.row(edgeIndex)(2) << ")" << std::endl;

  // Update F, E, EMAP, EF, EI.
  igl::edge_flaps(F, E, EMAP, EF, EI);
  // Update edge costs.
  calculate_optimal_edges_contraction();
  calculate_edges_cost();

  return true;
}

void BasicScene::simplify_edges(unsigned int edge_count_to_remove) {
  if (static_cast<std::size_t>(detailLevel) + 1 < meshes.size()) {
    detailLevel++;
    auto mesh = meshes.at(detailLevel);
    cube->SetMeshList({mesh});
    AddChild(cube);
    return;
  }
  bool something_collapsed = false;
  for (int j = 0; j < edge_count_to_remove; j++) {
    if (!collapse_edge()) {
      continue;
    }
    something_collapsed = true;
  }
  if (!something_collapsed && detailLevel) {
    std::cout << "Could not collapse any edges." << std::endl;
    return;
  }
  std::cout << "Collapsed edges." << std::endl;

  auto updatedTextureCoords = Eigen::MatrixXd::Zero(V.rows(), 2);
  Eigen::MatrixXd updatedVertexNormals;
  igl::per_vertex_normals(V, F, updatedVertexNormals);
  auto &meshData = cube->GetMeshList()[0]->data[0];
  auto newMesh = std::make_shared<cg3d::Mesh>(
      "cube", V, F, updatedVertexNormals, updatedTextureCoords);
  // Save new mesh.
  meshes.emplace_back(newMesh);
  detailLevel++;
  std::cout << "Current detail level: " << detailLevel << std::endl;
  cube->SetMeshList({newMesh});
  AddChild(cube);
}

void BasicScene::restore_edges() {
  if (detailLevel == 0) {
    std::cout << "Detail is at starting level and can not be increased"
              << std::endl;
    return;
  }
  detailLevel--;
  try {
    auto mesh = meshes.at(detailLevel);
    cube->SetMeshList({mesh});
    AddChild(cube);
  } catch (std::out_of_range) {
    // Reached maximum detail level (edges starting state), do nothing.
    detailLevel++;
  }
  std::cout << "Current detail level: " << detailLevel << std::endl;
}

void BasicScene::KeyCallback(Viewport *_viewport, int x, int y, int key,
                             int scancode, int action, int mods) {
  if (action == GLFW_PRESS || action == GLFW_REPEAT) {
    if (key == GLFW_KEY_SPACE || key == GLFW_KEY_DOWN) {
      const int edge_count_to_remove = std::ceil(0.1 * (E.rows()));
      simplify_edges(edge_count_to_remove);
    }

    if (key == GLFW_KEY_UP) {
      restore_edges();
    }
  }
}

void BasicScene::Update(const Program &program, const Eigen::Matrix4f &proj,
                        const Eigen::Matrix4f &view,
                        const Eigen::Matrix4f &model) {
  Scene::Update(program, proj, view, model);

  cube->Rotate(0.01f, Axis::XYZ);
}

Eigen::MatrixXi BasicScene::removeRow(Eigen::MatrixXi &matrix,
                                      unsigned int rowNum) {
  unsigned int numRows = matrix.rows() - 1;
  unsigned int numCols = matrix.cols();
  if (rowNum < numRows)
    matrix.block(rowNum, 0, numRows - rowNum, numCols) =
        matrix.block(rowNum + 1, 0, numRows - rowNum, numCols);
  matrix.conservativeResize(numRows, numCols);
  return matrix;
}

Eigen::MatrixXd BasicScene::removeRow(Eigen::MatrixXd &matrix,
                                      unsigned int rowNum) {
  unsigned int numRows = matrix.rows() - 1;
  unsigned int numCols = matrix.cols();
  if (rowNum < numRows)
    matrix.block(rowNum, 0, numRows - rowNum, numCols) =
        matrix.block(rowNum + 1, 0, numRows - rowNum, numCols);
  matrix.conservativeResize(numRows, numCols);
  return matrix;
}