#pragma once

#include "Scene.h"
#include <Eigen/Core>
#include <utility>
#include <igl/collapse_edge.h>

class BasicScene : public cg3d::Scene
{
public:
  explicit BasicScene(std::string name, cg3d::Display* display) : Scene(std::move(name), display) {};
  void Init(float fov, int width, int height, float near, float far);
  void Update(const cg3d::Program& program, const Eigen::Matrix4f& proj, const Eigen::Matrix4f& view, const Eigen::Matrix4f& model) override;
  void KeyCallback(cg3d::Viewport* _viewport, int x, int y, int key, int scancode, int action, int mods) override;

private:
    Eigen::MatrixXi E, EF, EI, F, OF;
    Eigen::MatrixXd faceNormals, V, OV;
    Eigen::VectorXi EQ, EMAP;
    Eigen::MatrixXd edgeContractions; // Row `i` holds contraction point of edge index `i` of matrix `E`.
    // The contraction point is a 3D vector representing a point which
    // the edge would contract to.
    std::vector<std::pair<double, std::size_t>> edgeCostsE;
    std::vector<std::pair<double, std::pair<std::size_t, std::size_t>>> edgeCosts; // (cost, (a_idx, b_dx)) pairs: cost is the
    // edge's cost, a_idx is the index of the edge's
    // first vertex, b_idx is the index of the edge's second vertex.
    std::vector<Eigen::Matrix4d> verticesQ; // Q matrix (from paper) of each vertex.
    // Row `i` has the Q matrix of vertex index `i` in matrix `V`.
    std::shared_ptr<cg3d::Model> cube;

  bool animate = false;
  int num_collapsed = 0;
  unsigned int detailLevel = 0;

  void calculate_Q_matrices();
  void calculate_optimal_edges_contraction();
  void calculate_edges_cost();
  void simplify_edges(unsigned int face_remove_count);
  void restore_edges();
  void SetActive(bool _isActive);
  void SetCamera(int index);
  bool collapse_edge();
  Eigen::MatrixXi removeRow(Eigen::MatrixXi& matrix, unsigned int rowNum);
  Eigen::MatrixXd removeRow(Eigen::MatrixXd& matrix, unsigned int rowNum);
  inline bool IsActive() const { return animate; };
  std::vector<std::shared_ptr<cg3d::Camera>> camList{ 4 };
  cg3d::Viewport* viewport = nullptr;
  std::vector<std::shared_ptr<cg3d::Mesh>> meshes;
};